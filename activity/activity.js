//Activity s30

//#1 ===========================

db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $count: "fruitsonSale"}
])

//#2 ===========================

db.fruits.aggregate([
	{ $match: { stock: {$gte: 20}}},
	{ $count: "enoughStock"}
])

//#3 ===========================

db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: {
		_id: "$supplier_id",
		avg_price: { $avg: "$price"}
		}
	}
])

//#4 ===========================

db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: {
		_id: "$supplier_id",
		max_price: { $max: "$price"}
                }
	},
	{$sort:{price: -1}}
	
])

//#5 ==========================

db.fruits.aggregate([
	{ $match: { onSale: true}},
	{ $group: {
		_id: "$supplier_id",
		min_price: { $min: "$price"}
                }
	},
	{$sort:{price: -1}}
	
])




















