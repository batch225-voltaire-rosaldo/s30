//Aggregation in MongoDb and Query Case Studies

//Inserted Data

db.fruits.insertMany([
		{
			name : "Apple",
			color : "Red",
			stock : 20,
			price: 40,
			supplier_id : 1,
			onSale : true,
			origin: [ "Philippines", "US" ]
		},

		{
			name : "Banana",
			color : "Yellow",
			stock : 15,
			price: 20,
			supplier_id : 2,
			onSale : true,
			origin: [ "Philippines", "Ecuador" ]
		},

		{
			name : "Kiwi",
			color : "Green",
			stock : 25,
			price: 50,
			supplier_id : 1,
			onSale : true,
			origin: [ "US", "China" ]
		},

		{
			name : "Mango",
			color : "Yellow",
			stock : 10,
			price: 120,
			supplier_id : 2,
			onSale : false,
			origin: [ "Philippines", "India" ]
		}     	
])

//Aggregation Pipeline

/*
	Documentation on aggregation
	https://www.mongodb.com/docs/manual/reference/operator/aggregation-pipeline/
*/

// 1 stage only ==================
//.count() - is for counting an object or specific elements of fields.

db.fruits.count();

// the $count stage returns a count of the remaining documents in the aggregation pipeline and assign the value to a field.

db.fruits.aggregate([
	{ $count: "fruits"}
]);

//2nd stage ========================

//with $match and $count
db.fruits.aggregate([
        {
            $match: { onSale: true }
        },
	{ $count: "fruitsonSale"}
]);

// $match and $group

db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: {$sum: "$stock" }
		}
	}
])






















